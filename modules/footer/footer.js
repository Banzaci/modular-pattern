"use strict";
(function(GB){

	GB.MODULES = GB.MODULES || {};

	GB.MODULES.FOOTER = (function(){

		var module = (function(){

			return {
				init:function(){
					console.log("init");
				},
				destroy:function(){
					
				}
			}

		}());

		return {
			init:module.init,
			destroy:module.destroy
		}

	});
	
}(GB = GB || {}));