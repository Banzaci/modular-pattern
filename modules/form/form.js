"use strict";
(function(GB){
	GB.MODULES = GB.MODULES || {};
	GB.MODULES.FORM = (function(sb){
		var module = (function(){
			return {
				init:function(){
					var submit = sb.$name("submit"),
						test = sb.$name("test");

					sb.addEventListener(submit, "click", module.postForm);
					//sb.html(test, sb.templater(test.innerHTML, "Kalle"))
					module.viewUsers();
				},
				destroy:function(){
					sb.removeEventListener(sb.$name("submit"), "click", module.postForm);
				},
				viewUsers:function(){
					sb.socket.emit({
						server: 'allUsers'
					}, function( response ){
						var ul = sb.$createElement("ul");
						ul.className = "users";
						response.forEach(function(record){
							ul.appendChild(sb.$createElementAndTextNode("li", record.firstName));
						});
						sb.element.appendChild(ul);
					});
				},
				postForm:function(e){

					e.preventDefault();
					
					var data = sb.validate(
						sb.$name("firstName"),
						sb.$name("lastName"),
						sb.$name("phone"),
						sb.$name("password"),
						sb.$name("email")
					);

					if(data){
						sb.socket.emit({
							server: 'addNewUser',
							user:data
						}, function( record ){
							if(!record.errors){
								var ul = sb.$(".users");
								ul.insertBefore(module.pushToList(record), ul.firstChild);
							}
						});
					}
				}
			}

		}());

		return {
			template:"modules/form/index.htm",
			init:module.init,
			destroy:module.destroy
		}

	});
	
}(GB = GB || {}));