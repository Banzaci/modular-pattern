(function(GB){

	GB.MODULES = GB.MODULES || {};

	GB.MODULES.MAINNAV = (function(){

		var _private = (function(){

			return {
				$:function( selector, element ){
					if(typeof selector === "string") {
						element = element || doc;
						return element.querySelector(selector);
					}
				}
			}

		}());

		return {
			$:_private.$
		}

	}());
	
}(GB = GB || {}));