"use strict";
(function(GB){

	GB.MODULES = GB.MODULES || {};

	GB.MODULES.BLOG = (function(sb){

		var module = (function(){

			return {
				init:function(){
					
					sb.addEventListener("button", "click", module.itemHasBeenClicked);
					
					sb.socket.emit({
						server: 'Post',
						data:123543543 
					}, function( data ){
						console.log( data );
					});

					sb.socket.on(function ( response ) {
          				console.log(response);
      				});

					sb.notify("read", {
						type:"read",
						data:"Hej"
					});
					//sb.appendDataToElement( data, view );		
					//sb.addEventListener(".btn", "click", module.itemHasBeenClicked);
				},
				destroy:function(){
					//sb.removeEventListener($button, "click", module.itemHasBeenClicked);
				},
				itemHasBeenClicked:function(e){
					
				}
			}

		}());

		return {
			template:"modules/blog/index.htm",
			init:module.init,
			destroy:module.destroy
		}

	});
	
}(GB = GB || {}));