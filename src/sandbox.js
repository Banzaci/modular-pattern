"use strict";
(function(GB){

	GB.SANDBOX = (function(){

		return {
			create:function( core, id, element ){
				return {
					element:element,
					removeClass:function(element, className){
						return core.removeClass(element, className);
					},
					addClass:function(element, className){
						return core.addClass(element, className);
					},
					listen:function( eventId, evts ){
						core.registerEvent( id, eventId, evts );
					},
					addEventListener:function( className, action, fn ){
						core.addEventListener( className, action, fn, element );
					},
					removeEventListener:function( className, action, fn ){
						core.removeEventListener( className, action, fn, element );
					},
					notify:function( evt, data ){
						core.trigger( evt, data );
					},
					rest:function( action, data ){
						core.rest( action, data );
					},
					html:function(element, html){
						return core.html(element, html);
					},
					socket:(function(){
						return {
							emit:function(data, fn){
								core.socket.emit( data, fn )
							},
							on:function(fn){
								core.socket.on( fn )
							}
						}
					}()),
					templater:function(html, data){
						return core.templater(html, data);
					},
					$:function( selector ){
						return core.$( selector, element );
					},
					$name:function( selector ){
						return core.$name( selector, element );
					},
					$attribute:function( element, funcs ){
						return core.$attribute( element, funcs );
					},
					$createElement:function( element ){
						return core.$createElement(element);
					},
					$createElementAndTextNode:function(element, text){
						return core.$createElementAndTextNode(element, text);
					},
					validate:function(){
						return core.validate( arguments );
					}
				}
			}
		}

	}());
	
}(GB = GB || {}));