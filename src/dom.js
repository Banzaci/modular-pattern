"use strict";
(function(GB){
	
	GB.DOM = (function(){

		var dom = (function(){
			var reg 	= new RegExp('^nn-', 'i'),
				core 	= GB.CORE,
				util	= "UTIL";

			return {
				removeClass:function(element, className){
					if (element.classList) {
						element.classList.remove(className);	
					} else {
						element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');	
					}
					return dom;
				},
				addClass:function(element, className){
					if (element.classList) {
						element.classList.add(className);
					} else {
						element.className += ' ' + className;
					}
					return dom;
				},
				$createElementAndTextNode:function(element, text){
					var li 		= core.$createElement(element),
						node 	= core.$createTextNode(text);
						
					li.appendChild(node);
					return li;
				},
				$clear:function(element){
					if(core.ns(util).isArray(element)) {
						element.forEach(function(e){
							e.innerHTML = null;
						});
					} else {
						element.innerHTML = null;
					}
					return dom;
				},
				remove:function(element){
					if(element !== null && element !== undefined){
						element.parentNode.removeChild(element);
					}
					return dom;
				},
				html:function(element, html){
					if(element !== null && element !== undefined){
						element.innerHTML = html;
					}
					return dom;
				},
				childNodesToList:function(childNodes){
					var children = [],
					    i = childNodes.length;

				    while (i--) {
				        if (childNodes[i].nodeType == 1) {
				            children.unshift(childNodes[i]);
				        }
				    }

				    return children;
				},
				findNNinDom:function(elements){
					var elm,
						attrs,
						attr,
						i,
						t = 0,
						attrName,
						module,
						arr = [],
						nodes,
						element;

					if(elements) {
						nodes = dom.childNodesToList(elements.childNodes);
						for(t; t < nodes.length; t++){
							element = nodes[t];
							if (element && element.hasAttribute) {
								attrs = element.attributes;
								i = 0;
								module = {};
								for(i; i<attrs.length; i++){
									attr = attrs[i];
									attrName = attr.name;

									if (reg.test(attrName)) {
										module[attrName.replace("nn-", "")] = core.$attribute(element, attrName);
									}
								}
								if(!core.ns(util).isObjectEmpty(module)) {
									arr.push({
										"element":element, 
										"attrs":module
									});
								}
							}
						}
					}
					return arr;
				},
				addEventListener:function(elements, evt, fn){
					if(typeof elements === "object"){
						if(elements.length) {
							dom.childNodesToList(elements).forEach(function(el){
								el.addEventListener(evt, fn);
							});	
						} else {
							elements.addEventListener(evt, fn);
						}
					} else {
						core.log("error", "Element is: " + elements);
					}
				},
				removeEventListener:function(elements, evt, fn){
					dom.childNodesToList(elements).forEach(function(element){
						element.removeEventListener(evt, fn);
					});
				}
			}
		}());

		return {
			removeClass:dom.removeClass,
			addClass:dom.addClass,
			addEventListener:dom.addEventListener,
			removeEventListener:dom.removeEventListener,
			$createElementAndTextNode:dom.$createElementAndTextNode,
			findNNinDom:dom.findNNinDom,
			html:dom.html,
			$:dom.$,
			$$:dom.$$,
			$attribute:dom.$attribute,
			childNodesToList:dom.childNodesToList,
			$clear:dom.$clear,
			remove:dom.remove
		}

	}());
	
}(GB = GB || {}));