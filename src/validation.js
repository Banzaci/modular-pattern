"use strict";
(function(GB){
	
	GB = GB || {};

	GB.VALIDATOR = (function(){

		var validator = (function(){
			return {
				validate:function(fns, element){
					var hasErrors = validator.hasErrors(fns, element).filter(function(error){
							return error !== "";
						});
						
					return (hasErrors.length > 0) ? hasErrors : element.value;
				},
				hasErrors:function(fns, element){
					return validator.check( fns, element ).map(function(error){
						return error;
					});
				},
				check:function(fns, element, err){
					var fn,
						a,
						error = err || [],
						fnName,
						value = element.value;

					if( fns.length === 0 ){
						return error;
					} else {
						
						fn 		= fns.shift();
						a 		= fn.replace("(", ",").replace(")", "").split(",");
						fnName 	= a.shift();

						if(typeof validator[fnName] === "function"){
							error.push(validator[fnName](a, element, value));
						}

						return validator.check(fns, element, error);
					}
				},
				min:function( args, element, value ){
					return (value.length < args[0]) ? { error:"För kort", element:element } : "";
				},
				max:function( args, element, value ){
					return (value.length > args[0]) ? { error:"För långt", element:element } : "";
				}
			}

		}());

		return {
			validate:validator.validate
		}

	}());
}(GB));