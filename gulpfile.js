var gulp 		= require('gulp')
	, concat 	= require('gulp-concat')
	, uglify 	= require('gulp-uglify')
  , jshint      = require('gulp-jshint')
  , rename      = require('gulp-rename')
  , clean       = require('gulp-clean')
  , ngAnnotate  = require('gulp-ng-annotate')
  , paths       = {
      scripts: [
                'src/core.js',
                'src/dom.js',
                'src/sandbox.js',
                'src/util.js',
                'src/logger.js',
                'src/facade.js',
                'src/rest.js',
                'src/validation.js',
                ],
      modules: [
                'modules/blog/*.js',
                'modules/form/*.js'
                ],
      app:'app/*.js',
      dest:[
                'dest/core.js',
                'dest/dom.js',
                'dest/sandbox.js',
                'dest/util.js',
                'dest/logger.js',
                'dest/facade.js',
                'dest/rest.js',
                'dest/router.js',
                'dest/validation.js',
                'dest/router.js',
                'dest/config.js',
                'dest/blog.js',
                'dest/form.js'
                ]
    };

gulp.task('default', function () {
    gulp.src('dest/*.js', {read: false}).pipe(clean())
    gulp.src('js/*.js', {read: false}).pipe(clean());
});

gulp.task('lint', function() {
  return gulp.src('./src/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('copy', function(){
    gulp.src(paths.modules).pipe(gulp.dest('dest/'))
    gulp.src(paths.app).pipe(gulp.dest('dest/'))
    gulp.src(paths.scripts).pipe(gulp.dest('dest/'));
});

gulp.task('scripts', function() {
    return gulp.src(paths.dest)
        .pipe(concat('main.js'))
        .pipe(ngAnnotate())
        .pipe(rename('main.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('js/'));
});

gulp.task('watch', function() {
    gulp.watch(paths.scripts, ['lint', 'scripts']);
});

gulp.task('default', ['copy', 'scripts']);//, 'watch', 'lint', 