"use strict";
(function(GB){
	
	GB.REST = (function(){

		var rest = (function(){
			
			var actions 	= ['get', 'post', 'put', 'delete'],
				facade 		= "FACADE",
				response 	= {},
				core 		= GB.CORE;

			return {
				call:function( action, data ){
					
					var request = core.request( action, data.data.url );

					core.onStateChange(request, function(resp){
						
						if(typeof data.fn === "function") {

							response = {
								status:"success",
								data:resp.response,
								modData:data
							}
						
							data.fn.call( data.core, response );	
						}
						
						request = null;
					},function(preload){
						//console.log(preload);
					},function(error){
						//console.log(error);
						request = null;
					}).send(data.message);
				},
				api:function( action, data ){
					if(typeof action === "string" && (~actions.indexOf(action.toLowerCase()))){
						rest.call(action, data);
					} else {
						console.log("Not valid action: " + action);
					}
				}
			}

		}());

		return {
			api:rest.api
		}

	}());
	
}(GB = GB || {}));