"use strict";
(function(GB){

	GB.MODULES = GB.MODULES || {};

	GB.MODULES.BLOG = (function(sb){

		var module = (function(){

			return {
				init:function(){
					sb.addEventListener("button", "click", module.itemHasBeenClicked);
					// sb.rest("post", {
					// 	url:"http://localhost:3000/ms/",
					// 	template:null,
					// 	data:{"message":12345}
					// }, function(data){
					// 	console.log(data);
					// });

					sb.notify("read", {
						type:"read",
						data:"Hej"
					});
					//sb.appendDataToElement( data, view );		
					//sb.addEventListener(".btn", "click", module.itemHasBeenClicked);
					//Not append event on all children, "event delegation"
				},
				destroy:function(){
					//sb.removeEventListener($button, "click", module.itemHasBeenClicked);
				},
				itemHasBeenClicked:function(e){
					
				}
			}

		}());

		return {
			template:"modules/blog/index.htm",
			init:module.init,
			destroy:module.destroy
		}

	});
	
}(GB = GB || {}));