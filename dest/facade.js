"use strict";
(function(GB){

	GB.FACADE = (function(){

		var facade = (function(){

			return {
				onStateChange:function(request, success, preloader, error){
					var loader;
					xhr.onreadystatechange = function () {
						loader = (xhr.readyState/4) * 100;
						if (xhr.readyState === 4){
							if(xhr.status === 200){
								success(xhr);
							} else {
								error(xhr.responseText);
							}
							xhr = null;
						}
						preloader(loader);
					};
					return xhr;
				}
			}

		}());

		return {
			xhr:facade.xhr,
			onStateChange:facade.onStateChange
		}

	}());
	
}(GB = GB || {}));