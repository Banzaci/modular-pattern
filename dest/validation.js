"use strict";
(function(GB){
	
	GB = GB || {};

	GB.VALIDATOR = (function(){

		var validator = (function(){

			return {
				validate:function(fns, value){
					var hasErrors = validation.hasErrors(fns, value).filter(function(error){
						return error !== "";
					});
					return hasErrors.length > 0 ? hasErrors : false;
				},
				hasErrors:function(fns, value){
					return validation.check( fns, value ).map(function(error){
						return error;
					});
				},
				check:function(fns, value, err){
					var fn,
						a,
						error = err || [];
					if( fns.length === 0 ){
						return error;
					} else {
						fn = fns.shift();
						a = fn.replace("(", ",").replace(")", "").split(",");
						fnName = a.shift();
						if(typeof validation[fnName] === "function"){
							error.push(validation[fnName](a, value));
						}
						return validation.check(fns, value, error);
					}
				},
				min:function( args, value ){
					return (value.length < args[0]) ? "För kort" : "";
				},
				max:function( args, value ){
					return (value.length > args[0]) ? "För långt" : "";
				}
			}

		}());

		return {
			validate:validator.validate
		}

	}());
}(GB));