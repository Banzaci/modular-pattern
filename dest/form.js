"use strict";
(function(GB){

	GB.MODULES = GB.MODULES || {};

	GB.MODULES.FORM = (function(sb){

		var module = (function(){

			return {
				init:function(){
					sb.listen("read", {
						type:"read",
						data:"Hej",
						fn:function(data){
							console.log(data);
						}
					});
					//sb.appendDataToElement( data, view );		
					//sb.addEventListener(".btn", "click", module.itemHasBeenClicked);
					//Not append event on all children, "event delegation"
				},
				destroy:function(){
					//sb.removeEventListener($button, "click", module.itemHasBeenClicked);
				},
				itemHasBeenClicked:function(e){
					
				}
			}

		}());

		return {
			init:module.init,
			destroy:module.destroy
		}

	});
	
}(GB = GB || {}));