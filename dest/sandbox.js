"use strict";
(function(GB){

	GB.SANDBOX = (function(){

		return {
			create:function( core, id, element ){
				return {
					listen:function( eventId, evts ){
						core.registerEvent( id, eventId, evts );
					},
					addEventListener:function( className, action, fn ){
						core.addEventListener( className, action, fn, element );
					},
					removeEventListener:function( className ){
						core.removeEventListener( className, action, fn, element );
					},
					notify:function( evt, data ){
						core.trigger( evt, data );
					},
					rest:function( action, data, fn ){
						core.rest( action, data, fn );
					},
					$:function( selector ){
						return core.$( selector, element );
					},
					$name:function( selector ){
						return core.$name( selector, element );
					},
					$attribute:function( element, funcs ){
						return core.$attribute( element, funcs );
					},
					$validater:function( element, funcs ){
						return core.$validater( element, funcs );
					},
					validate:function( funcs, text ){
						return core.validate( funcs, text );
					}
				}
			}
		}

	}());
	
}(GB = GB || {}));