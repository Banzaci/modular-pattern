(function(GB){
	var view 	= GB.CORE.$template("view"),
		win 	= window;

	GB.CORE.route('/', '/templates/home/index.htm', view);  
	GB.CORE.route('/about', '/templates/about/index.htm', view);
	
}(GB = GB || {}));