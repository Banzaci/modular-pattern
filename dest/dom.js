"use strict";
(function(GB){
	
	GB.DOM = (function(){

		var dom = (function(){
			var reg 	= new RegExp('^nn-', 'i'),
				core 	= GB.CORE,
				util	= "UTIL";

			return {
				clear:function(element){
					if(core.ns(util).isArray(element)) {
						element.forEach(function(e){
							e.innerHTML = null;
						});
					} else {
						element.innerHTML = null;
					}
					return this;
				},
				remove:function(element){
					if(element !== null && element !== undefined){
						element.parentNode.removeChild(element);
					}
					return this;
				},
				html:function(element, html){
					if(element !== null && element !== undefined){
						element.innerHTML = html;
					}
					return this;
				},
				childNodesToList:function(childNodes){
					var children = [],
					    i = childNodes.length;

				    while (i--) {
				        if (childNodes[i].nodeType == 1) {
				            children.unshift(childNodes[i]);
				        }
				    }

				    return children;
				},
				findNNinDom:function(elements){
					var elm,
						attrs,
						attr,
						i,
						t = 0,
						attrName,
						module,
						arr = [],
						nodes,
						element;

					if(elements) {
						nodes = dom.childNodesToList(elements.childNodes);
						for(t; t < nodes.length; t++){
							element = nodes[t];
							if (element && element.hasAttribute) {
								attrs = element.attributes;
								i = 0;
								module = {};
								for(i; i<attrs.length; i++){
									attr = attrs[i];
									attrName = attr.name;

									if (reg.test(attrName)) {
										module[attrName.replace("nn-", "")] = core.$attribute(element, attrName);
									}
								}
								if(!core.ns(util).isObjectEmpty(module)) {
									arr.push({
										"element":element, 
										"attrs":module
									});
								}
							}
						}
					}
					return arr;
				},
				addEventListener:function(elements, evt, fn){
					dom.childNodesToList(elements).forEach(function(el){
						el.addEventListener(evt, fn);
					});
				},
				removeEventListener:function(element, evt, fn){
					element.removeEventListener(evt, fn);
				}
			}

		}());

		return {
			addEventListener:dom.addEventListener,
			removeEventListener:dom.removeEventListener,
			findNNinDom:dom.findNNinDom,
			html:dom.html,
			$:dom.$,
			$$:dom.$$,
			$attribute:dom.$attribute,
			clear:dom.clear,
			remove:dom.remove
		}

	}());
	
}(GB = GB || {}));