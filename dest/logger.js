"use strict";
(function(GB){
	GB.LOGGER = (function(){
		var logger = (function(){
			return {
				server:function( message ){
					
					var logUrl = "http://localhost:3000/ms";
					
					GB.CORE.request("post", {
						core:logger,
						data:{
							url:this.logUrl,
							data:message
						},
						message:"message",
							fn:function(response){
								console.log(response);
							}
					} );
					
				},
				info:function( message, time ){
					time = time ? new Date().toString("dd/MM/yyyy HH:mm:ss fff") : "";
					console.log(message, time);
				},
				warn:function( message ){
					console.warn(message,new Date().toString("dd/MM/yyyy HH:mm:ss fff"));
				},
				error:function( message ){
					console.error(message,new Date().toString("dd/MM/yyyy HH:mm:ss fff"));
				},
				trace:function( message ){
					console.trace(message);
				},
				time:function( name ){
					console.time(name);	
				},
				timeEnd:function( name ){
					console.timeEnd(name);
				}
			}

		}());

		return {
			server:logger.server,
			info:logger.info,
			warn:logger.warn,
			error:logger.error,
			trace:logger.trace,
			time:logger.time,
			timeEnd:logger.timeEnd
		}

	}());
	
}(GB = GB || {}));