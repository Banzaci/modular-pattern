"use strict";
(function(GB){

	GB.UTIL = (function(){

		return {
			isObjectEmpty:function(obj){
	    		return Object.keys(obj).length === 0;
	    	},
	    	isDomObject:function(element){
	    		return (typeof element==="object") && (element.nodeType===1) && (typeof element.tagName==="string");
	    	},
	    	isString:function(obj){
				return (typeof obj === 'string');
	    	},
	    	extend:function(parent, child){
				var i,
					toStr = Object.prototype.toString,
					astr = "[object Array]",
					self = this;

				child = child || {};

				for(i in parent){
					if(parent.hasOwnProperty(i)){
						if(typeof parent[i] === "object"){
							child[i] = (toStr.call(parent[i]) === astr) ? [] : parent[i];
							self.call(self, extend(parent[i], child[i]));
						} else {
							child[i] = parent[i]; 
						}
					}
				}
				
				return child;
			},
			copy:function(parent){
				var child = {};
				if(typeof parent !== "object") { throw new Error("Object is required."); return; }
				for(prop in parent){
					if(parent.hasOwnProperty(prop)){
	                  	child[prop] = parent[prop]; 
					}
				}

				return child;
			},
			quicksort:function(arr) {
			    
			    if (typeof parent !== "array" && arr.length == 0) { throw new Error("Array is required, or is empty."); return; }
			 
			    var left = [],
			    	right = [],
			    	pivot = arr[0];
			 
			    for (var i = 1; i < arr.length; i++) {
			        if (arr[i] < pivot) {
			           left.push(arr[i]);
			        } else {
			           right.push(arr[i]);
			        }
			    }
			 
			    return quicksort(left).concat(pivot, quicksort(right));
			},
			flatten:function(obj, into, prefix) {
			    var prop;
			    into = into || {};
			    prefix = prefix || "";

			    for (var k in obj) {
			        if (obj.hasOwnProperty(k)) {
			            prop = obj[k];
			            if (prop && typeof prop === "object") {
			                this.flatten(prop, into, prefix + k + ".");
			            }
			            else {
			                into[prefix + k] = prop;
			            }
			        }
			    }
			    return into;
		    },
		    operator:function(op, item, filter){
				switch(op){
					case "===": return item === filter;
					case "!==": return item !== filter;
					case "<": return item < filter;
					case ">": return item > filter;
				}
		    },
		    filter:function(arr, op, filter){
		        var self = this;
		    	return arr.filter(function(item){
		    		return self.operator(op, item, filter);
		    	});
		    },
		    map:function(arr, fn){
		        return arr.map(fn);
		    },
		    isArray:function(arr){
		    	return (Object.prototype.toString.call(arr) === '[object Array]');
		    },
		    isPlainObject:function(obj){
				return (Object.prototype.toString.call(obj) === '[object Object]');
		    },
		    isObject:function(obj){
				return (typeof obj === 'object');
		    },
		    flatMap:function(arr){
		    	var head,
		    		tail,
		    		self = this;

				if (self.isArray(arr) && arr.length > 0) {
					head = arr[0];
					tail = arr.slice(1);

					return self.flatten(head).concat(self.flatten(tail));
				} else {
					return [].concat(arr);
				}
		    }
		}
	}());

}(GB = GB || {}));