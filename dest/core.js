'use strict';
var GB = {};
(function(GB){
	
	GB.CORE = (function(){
		
		var core = (function(){
			
			var util 		= 'UTIL',
				dom 		= 'DOM',
				facade 		= 'FACADE',
				sandbox 	= 'SANDBOX',
				rest 		= 'REST',
				log 		= 'LOGGER',
				modules 	= 'MODULES',
				validator 	= 'VALIDATOR',
				PREFIX 		= 'nn-',
				debug		= true,
				routes 		= {},
				doc 		= document,
				modulesPath = '../modules/';

			return {
				ns:function(obj){
					try {
						if(typeof GB[obj] === 'object') {
							return GB[obj];	
						} else {
							core.log( 'warn', obj + ' does not exist!', true );
						}
					} catch(e){
						core.log( 'warn', e.message, true );
					}
				},
				request:function(action, url ){
					var xhr; 
					if (window.XMLHttpRequest) {
				        xhr = new XMLHttpRequest();
				    }
				    else {
				        xhr = new ActiveXObject("Microsoft.XMLHTTP");
				    }
					xhr.open(action, url, true);
					return xhr;
				},
				onStateChange:function(xhr, success, preloader, error){
					var loader;
					xhr.onreadystatechange = function () {
						loader = (xhr.readyState/4) * 100;
						if (xhr.readyState === 4){
							if(xhr.status === 200){
								success(xhr);
							} else {
								error(xhr.responseText);
							}
							xhr = null;
						}
						preloader(loader);
					};
					return xhr;
				},
				log:function(type, message, time){
					if(debug) {
						try {
							core.ns(log)[type](message, time);
						} catch(e){
							console.log(e.message);
						}
					}
				},
				$:function( selector, element ){
					if(typeof selector === "string") {
						element = element || doc;
						return element.querySelector(selector);
					}
				},
				$$:function(selectors, element){
					element = element || doc;
					if(typeof selectors === "string") {
						return element.querySelectorAll(selectors);
					}
				},
				$attribute:function(element, attr){
					if(typeof attr === "string") {
						element = element || doc;
						return element.getAttribute(attr);
					}
				},
				$name:function( selector, element ){
					return core.$('['+PREFIX+'name="'+selector+'"]', element);
				},
				$validater:function(element, selector){
					return core.$attribute( element, PREFIX + selector );
				},
				$template:function(selector){
					return core.$('['+PREFIX+'template="'+selector+'"]');
				},
				$clearTemplates:function(){
					core.ns(dom).clear([
						core.$template('header'), 
						core.$template('view'), 
						core.$template('footer')
					]);
					return core;
				},
				$loadTemplate:function( data, fn ){
					core.clearCurrentGroup();
					core.rest( 'get', data, fn );
					return core;
				},
				rest:function( action, data, fn){
					core.ns(rest).api(action, {
						core:core, 
						fn:fn,
						data:data
					});
				},
				router:function(){
					var url 	= location.hash.slice(1) || 'index',
						route 	= routes[url];
					return route;
				},
				route:function( path, templateUrl, pageTemplate ){
					path = path.slice(1) || 'index';
					routes[path] = {
						url:templateUrl,
						group:path,
						container:pageTemplate,
						modules:{}
					};
				},
				templateResponse:function(data){

					var mod,
						thisGroup,
						moduleId,
						route;

					if(data.status === 'success') {
						
						mod = data.modData.data;
						core.ns(dom).html( mod.container, data.data.trim() );
						
						route = core.router();
						
						if( mod.group ){
							if(route.modules.length){
								thisGroup = route.modules;
								thisGroup.forEach(function(module, index){
									//console.log(module, index);
									//core.startModule(index);
								})
							} else {
								route.modules = core.ns(dom).findNNinDom( mod.container );
								core.loadModule(route, 0);
							};
						} else {
							core.loadModule( core.ns(dom).findNNinDom( mod.container ), 0 );
						};
					}
				},
				loadModule:function( temp, index ){
					var current,
						id,
						mod,
						attrs,
						element,
						self = core,
						mods = temp.modules ? temp.modules : temp;
					
					if(core.ns(util).isArray(mods)){
						
						if(index === mods.length) {
							core.log('info', 'Finished loading all modules! ', true);
							return;
						}
						
						current = mods[index];
						attrs 	= current.attrs;
						id 		= attrs.init
						element = current.element;
						mod 	= core.ns(modules)[id.toUpperCase()];
						
						if(typeof mod === 'function') {
							current.create = mod;
							self.startModule(current, index, id);
							index = index + 1;
							self.loadModule(mods, index);
						} else {
							core.log('warn', 'Module ' + id + ' is not a function!', false);
						}
						
					} else {
						core.log('warn', 'Module are no array!', false);
					}
				},
				startModule:function(mod, index, id){
					var template;
					
					if(typeof mod === 'object') {
						
						mod.instance = mod.create(core.ns(sandbox).create(core, id, mod.element));
						mod.instance.element = mod.element;
						mod.instance.conditions = mod.conditions;

						if(mod.instance.template) {
							core.rest( 'get', { url:mod.instance.template, container:mod.instance.element }, function(data){
								if(data.status === 'success') {
									template = data.modData.data;
									core.ns(dom).clear( template.container ).html( template.container, data.data.trim() );
									mod.instance.init();
								}
							});
						} else {
							mod.instance.init();
						}
					}
				},
				clearCurrentGroup:function(){
					var toDestroy = core.router(),
						mod,
						thisGroup;

					if(toDestroy !== undefined) {
						thisGroup = toDestroy.modules;
						if(core.ns(util).isArray(thisGroup)){
							thisGroup.forEach(function(elem){
								console.log(elem);
							});
						}
					}
					return core;
				},
				moduleById:function(id){
					var route = core.router(),
							module;
					route.modules.forEach(function(mod){
						if(mod.attrs && mod.attrs.init === id) {
							module = mod;
						}
					});
					return module;
				},
				destroy:function(id){
					var mod = core.moduleById(id);
					if(mod) {
						mod.instance.destroy();
						mod.instance = null;
					}
				},
				trigger:function( evt, data ){
					var route = core.router();
					route.modules.forEach(function(mod){
						if(mod.events && mod.events[evt]) {
							mod.events[ evt ].fn.call( mod.instance, data );
						}
					});
				},
				registerEvent:function( id, eventId, evts ){
					var mod = core.moduleById(id);
					if(mod) {
						mod.events = mod.events || {}
						mod.events[eventId] = evts;
					}
				},
				addEventListener:function( selector, evt, fn, element ){
					core.ns(dom).addEventListener(core.$$(selector, element), evt, fn);
				},
				validate:function( funcs, value ){
					return core.ns(validator).validate( funcs.split('|'), value );
				}
			}

		}());

		return {
			request:core.request,
			ns:core.ns,
			router:core.router,
			addEventListener:core.addEventListener,
			$clearTemplates:core.$clearTemplates,
			$template:core.$template,
			$loadTemplate:core.$loadTemplate,
			$attribute:core.$attribute,
			route:core.route,
			trigger:core.trigger,
			log:core.log,
			onStateChange:core.onStateChange,
			rest:core.rest,
			listen:core.registerEvent,
			clearCurrentGroup:core.clearCurrentGroup,
			templateResponse:core.templateResponse
		}
		
	}());

}(GB = GB || {}));