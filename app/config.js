(function(GB){
	
	var view 	= GB.CORE.router();
	view.fn 	= GB.CORE.templateResponse;

	GB.CORE.$clearTemplates()//Skicka in som objeck
		.$loadTemplate( 
		{ 
			container:GB.CORE.$template("header"),
			url:"/modules/mainNav/index.htm",
			fn:GB.CORE.templateResponse
		})
		.$loadTemplate( 
		{ 
			container:GB.CORE.$template("footer"),
			url:"/modules/footer/index.htm",
			fn:GB.CORE.templateResponse 
		}).$loadTemplate( view );
}(GB = GB || {}));
